/*
 * Copyright 2008 Andrew Riedi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <stdio.h>
#include <string.h>
#include <windows.h>
#include "reg.h"

static int output_string(int msg, ...)
{
    WCHAR msg_buffer[8192];
    va_list arguments;

    LoadString(GetModuleHandle(NULL), msg, msg_buffer, sizeof(msg_buffer));
    va_start(arguments, msg);
    vwprintf(msg_buffer, arguments);
    va_end(arguments);

    return 0;
}

static int reg_add(WCHAR *key_name, WCHAR *value_name, BOOL value_empty,
    WCHAR *type, WCHAR separator, WCHAR *data, BOOL force)
{
    const WCHAR stubW[] = {'S','T','U','B',' ','A','D','D',' ','-',' ','%','s',
        ' ','%','s',' ','%','d',' ','%','s',' ','%','c',' ','%','s',' ','%',
        'd','\n',0};
    fwprintf(stderr, stubW, key_name, value_name, value_empty, type, separator, data, force);

    return 1;
}

static int reg_delete(WCHAR *key_name, WCHAR *value_name, BOOL value_empty,
    BOOL value_all, BOOL force)
{
    const WCHAR stubW[] = {'S','T','U','B',' ','D','E','L','E','T','E',' ','-',
        ' ','%','s',' ','%','s',' ','%','d',' ','%','d',' ','%','d','\n',0};
    fwprintf(stderr, stubW, key_name, value_name, value_empty, value_all, force);

    return 1;
}

static int reg_query(WCHAR *key_name, WCHAR *value_name, BOOL value_empty,
    BOOL subkey)
{
    const WCHAR stubW[] = {'S','T','U','B',' ','Q','U','E','R','Y',' ','-',' ',
        '%','s',' ','%','s',' ','%','d',' ','%','d','\n',0};
    fwprintf(stderr, stubW, key_name, value_name, value_empty, subkey);

    return 1;
}

int wmain(int argc, WCHAR *argvW[])
{
    int i;

    const WCHAR addW[] = {'a','d','d',0};
    const WCHAR deleteW[] = {'d','e','l','e','t','e',0};
    const WCHAR queryW[] = {'q','u','e','r','y',0};
    const WCHAR slashDW[] = {'/','d',0};
    const WCHAR slashFW[] = {'/','f',0};
    const WCHAR slashSW[] = {'/','s',0};
    const WCHAR slashTW[] = {'/','t',0};
    const WCHAR slashVW[] = {'/','v',0};
    const WCHAR slashVAW[] = {'/','v','a',0};
    const WCHAR slashVEW[] = {'/','v','e',0};

    if (argc < 2)
    {
        output_string(STRING_USAGE);
        return 1;
    }

    if (!_wcsicmp(argvW[1], addW))
    {
        WCHAR *key_name, *value_name = NULL, *type = NULL, separator = '\0', *data = NULL;
        BOOL value_empty = FALSE, force = FALSE;

        if (argc < 3)
        {
            output_string(STRING_ADD_USAGE);
            return 1;
        }

        key_name = argvW[2];
        for (i = 1; i < argc; i++)
        {
            if (!_wcsicmp(argvW[i], slashVW))
                value_name = argvW[++i];
            else if (!_wcsicmp(argvW[i], slashVEW))
                value_empty = TRUE;
            else if (!_wcsicmp(argvW[i], slashTW))
                type = argvW[++i];
            else if (!_wcsicmp(argvW[i], slashSW))
                separator = argvW[++i][0];
            else if (!_wcsicmp(argvW[i], slashDW))
                data = argvW[++i];
            else if (!_wcsicmp(argvW[i], slashFW))
                force = TRUE;
        }
        return reg_add(key_name, value_name, value_empty, type, separator,
            data, force);
    }
    else if (!_wcsicmp(argvW[1], deleteW))
    {
        WCHAR *key_name, *value_name = NULL;
        BOOL value_empty = FALSE, value_all = FALSE, force = FALSE;

        if (argc < 3)
        {
            output_string(STRING_DELETE_USAGE);
            return 1;
        }

        key_name = argvW[2];
        for (i = 1; i < argc; i++)
        {
            if (!_wcsicmp(argvW[i], slashVW))
                value_name = argvW[++i];
            else if (!_wcsicmp(argvW[i], slashVEW))
                value_empty = TRUE;
            else if (!_wcsicmp(argvW[i], slashVAW))
                value_all = TRUE;
            else if (!_wcsicmp(argvW[i], slashFW))
                force = TRUE;
        }
        return reg_delete(key_name, value_name, value_empty, value_all, force);
    }
    else if (!_wcsicmp(argvW[1], queryW))
    {
        WCHAR *key_name, *value_name = NULL;
        BOOL value_empty = FALSE, subkey = FALSE;

        if (argc < 3)
        {
            output_string(STRING_QUERY_USAGE);
            return 1;
        }

        key_name = argvW[2];
        for (i = 1; i < argc; i++)
        {
            if (!_wcsicmp(argvW[i], slashVW))
                value_name = argvW[++i];
            else if (!_wcsicmp(argvW[i], slashVEW))
                value_empty = TRUE;
            else if (!_wcsicmp(argvW[i], slashSW))
                subkey = TRUE;
        }
        return reg_query(key_name, value_name, value_empty, subkey);
    }

    return 0;
}
